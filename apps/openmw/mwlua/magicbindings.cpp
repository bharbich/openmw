#include "apps/openmw/mwbase/environment.hpp"
#include "apps/openmw/mwbase/world.hpp"
#include "apps/openmw/mwlua/localscripts.hpp"
#include "apps/openmw/mwlua/luabindings.hpp"
#include "apps/openmw/mwlua/luamanagerimp.hpp"
#include "apps/openmw/mwlua/object.hpp"
#include "apps/openmw/mwlua/worldview.hpp"
#include "apps/openmw/mwmechanics/activespells.hpp"
#include "apps/openmw/mwmechanics/creaturestats.hpp"
#include "apps/openmw/mwmechanics/npcstats.hpp"
#include "apps/openmw/mwworld/action.hpp"
#include "apps/openmw/mwworld/class.hpp"
#include "apps/openmw/mwworld/esmstore.hpp"
#include "components/esm3/activespells.hpp"
#include "components/esm3/loadmgef.hpp"
#include "components/esm3/loadspel.hpp"
#include "components/lua/luastate.hpp"
#include "components/misc/resourcehelpers.hpp"
#include "components/resource/resourcesystem.hpp"

namespace MWLua
{
    using SelfObject = MWLua::LocalScripts::SelfObject;
    using MagicObject = std::variant<SelfObject*, MWLua::LObject, MWLua::GObject>;

    // for functions which require 'self' context
    SelfObject* asSelfObject(const MagicObject& obj)
    {
        if (!std::holds_alternative<SelfObject*>(obj))
            throw std::runtime_error("Operation is only permitted in local scripts for 'openmw.self'.");
        return std::get<SelfObject*>(obj);
    }

    // for functions which require either 'self' or 'global' context
    MWWorld::Ptr asSelfOrGlobal(const MagicObject& obj)
    {
        if (std::holds_alternative<LObject>(obj))
            throw std::runtime_error(
                "Operation is only permitted in local scripts for 'openmw.self', or in global scripts");
        return std::holds_alternative<GObject>(obj) ? std::get<GObject>(obj).ptr() : std::get<SelfObject*>(obj)->ptr();
    }

    const MWLua::Object* getObject(const MagicObject& obj)
    {
        return std::visit(
            [](auto&& variant) -> const MWLua::Object* {
                using T = std::decay_t<decltype(variant)>;
                if constexpr (std::is_same_v<T, SelfObject*>)
                    return variant;
                else if constexpr (std::is_same_v<T, MWLua::LObject>)
                    return &variant;
                else if constexpr (std::is_same_v<T, MWLua::GObject>)
                    return &variant;
            },
            obj);
    }

    // class returned via 'types.Actor.spells(obj)' in lua
    class SpellsAccessor
    {
        const MagicObject mObj;
        std::vector<const ESM::Spell*> mSpells;

        SpellsAccessor(MagicObject object)
            : mObj(std::move(object))
        {
        }

    public:
        static std::optional<SpellsAccessor> create(MagicObject object)
        {
            if (!getObject(object)->ptr().getClass().isActor())
                return {};
            return SpellsAccessor{ std::move(object) };
        }

        size_t count() const
        {
            const auto& ptr = getObject(mObj)->ptr();
            return ptr.getClass().getCreatureStats(ptr).getSpells().count();
        }

        void add(const std::string_view id, const MWLua::Context& context)
        {
            const auto& ptr = asSelfOrGlobal(mObj);
            const ESM::RefId spellId = ESM::RefId::stringRefId(id);
            context.mLuaManager->addAction(
                [ptr, spellId]() { ptr.getClass().getCreatureStats(ptr).getSpells().add(spellId); });
        }

        void remove(const std::string_view id, const MWLua::Context& context)
        {
            const auto& ptr = asSelfOrGlobal(mObj);
            const ESM::RefId spellId = ESM::RefId::stringRefId(id);
            context.mLuaManager->addAction(
                [ptr, spellId]() { ptr.getClass().getCreatureStats(ptr).getSpells().remove(spellId); });
        }

        void clear(const MWLua::Context& context)
        {
            const auto& ptr = asSelfOrGlobal(mObj);
            context.mLuaManager->addAction([ptr]() { ptr.getClass().getCreatureStats(ptr).getSpells().clear(); });
        }

        const ESM::Spell* getAt(size_t index) const
        {
            const auto& ptr = getObject(mObj)->ptr();
            return ptr.getClass().getCreatureStats(ptr).getSpells().getAt(index);
        }

        const ESM::Spell* getAt(const std::string_view id) const
        {
            const auto& ptr = getObject(mObj)->ptr();
            const MWWorld::Store<ESM::Spell>* store
                = &MWBase::Environment::get().getWorld()->getStore().get<ESM::Spell>();
            const ESM::RefId refId = ESM::RefId::stringRefId(id);
            const ESM::Spell* spell = store->find(refId);
            if (!ptr.getClass().getCreatureStats(ptr).getSpells().hasSpell(spell))
                throw std::runtime_error("Invalid index");
            return spell;
        }
    };

    // class returned via 'types.Actor.activeSpells(obj)' in lua
    class ActiveSpellsAccessor
    {
        MagicObject mObj;
        ActiveSpellsAccessor(MagicObject object)
            : mObj(std::move(object))
        {
        }

    public:
        static std::optional<ActiveSpellsAccessor> create(MagicObject object)
        {
            if (!getObject(object)->ptr().getClass().isActor())
                return {};
            return ActiveSpellsAccessor{ std::move(object) };
        }

        size_t count() const
        {
            const auto& ptr = getObject(mObj)->ptr();
            return ptr.getClass().getCreatureStats(ptr).getActiveSpells().count();
        }

        void remove(const std::string_view id, const MWLua::Context& context)
        {
            const auto& ptr = asSelfOrGlobal(mObj);
            const ESM::RefId spellId = ESM::RefId::stringRefId(id);
            context.mLuaManager->addAction([ptr, spellId]() {
                ptr.getClass().getCreatureStats(ptr).getActiveSpells().removeEffects(ptr, spellId);
            });
        }

        void clear(const MWLua::Context& context)
        {
            const auto& ptr = asSelfOrGlobal(mObj);
            context.mLuaManager->addAction(
                [ptr]() { ptr.getClass().getCreatureStats(ptr).getActiveSpells().clear(ptr); });
        }

        const MWMechanics::ActiveSpells::ActiveSpellParams getAt(size_t index) const
        {
            const auto& ptr = getObject(mObj)->ptr();
            return ptr.getClass().getCreatureStats(ptr).getActiveSpells().getAt(index);
        }
    };

    template <class T>
    auto addAccessor()
    {
        return sol::overload([](MWLua::LocalScripts::SelfObject& o) { return T::create(&o); },
            [](const MWLua::LObject& o) { return T::create(o); }, [](const MWLua::GObject& o) { return T::create(o); });
    }

    void addActorMagicBindings(sol::table& actor, const Context& context)
    {
        // types.Actor.spells(o)
        actor["spells"] = addAccessor<SpellsAccessor>();
        auto spellsT = context.mLua->sol().new_usertype<SpellsAccessor>("Spells");
        spellsT[sol::meta_function::to_string] = [](const MWLua::SpellsAccessor& spells) {
            size_t len = spells.count();
            return "{" + std::to_string(len) + " " + "spell" + (len == 1 ? "" : "s") + "}";
        };

        // types.Actor.spells(o).count
        spellsT[sol::meta_function::length] = spellsT["count"].template get<sol::function>();

        // types.Actor.spells(o)[i]
        spellsT[sol::meta_function::index] = sol::overload(
            // indexing spells by integer:
            [](const MWLua::SpellsAccessor& spells, size_t index) { return spells.getAt(index - 1); },
            // indexing spells by string:
            [](const MWLua::SpellsAccessor& spells, const std::string_view index) { return spells.getAt(index); });

        // pairs(types.Actor.spells(o))
        spellsT[sol::meta_function::pairs] = context.mLua->sol()["ipairsForArray"].template get<sol::function>();

        // ipairs(types.Actor.spells(o))
        spellsT[sol::meta_function::ipairs] = context.mLua->sol()["ipairsForArray"].template get<sol::function>();

        // types.Actor.spells(o):add(id)
        spellsT["add"] = [context](MWLua::SpellsAccessor& spells, const std::string_view spellId) {
            spells.add(spellId, context);
        };

        // types.Actor.spells(o):remove(id)
        spellsT["remove"] = [context](MWLua::SpellsAccessor& spells, const std::string_view spellId) {
            spells.remove(spellId, context);
        };

        // types.Actor.spells(o):clear()
        spellsT["clear"] = [context](MWLua::SpellsAccessor& spells) { spells.clear(context); };

        // types.Actor.activeSpells(o)
        actor["activeSpells"] = addAccessor<ActiveSpellsAccessor>();
        auto activeSpellsT = context.mLua->sol().new_usertype<ActiveSpellsAccessor>("ActiveSpells");
        activeSpellsT[sol::meta_function::to_string] = [](const MWLua::ActiveSpellsAccessor& spells) {
            size_t len = spells.count();
            return "{" + std::to_string(len) + " " + "spell" + (len == 1 ? "" : "s") + "}";
        };

        // types.Actor.activeSpells(o).count
        activeSpellsT[sol::meta_function::length] = activeSpellsT["count"].template get<sol::function>();

        // # types.Actor.activeSpells(o)[i]
        activeSpellsT[sol::meta_function::index] = [](const MWLua::ActiveSpellsAccessor& activeSpells, size_t index) {
            return activeSpells.getAt(index - 1);
        };

        // pairs(types.Actor.activeSpells(o))
        activeSpellsT[sol::meta_function::pairs] = context.mLua->sol()["ipairsForArray"].template get<sol::function>();

        // ipairs(types.Actor.activeSpells(o))
        activeSpellsT[sol::meta_function::ipairs] = context.mLua->sol()["ipairsForArray"].template get<sol::function>();

        // types.Actor.activeSpells(o):remove(id)
        activeSpellsT["remove"] = [context](MWLua::ActiveSpellsAccessor& activeSpells, const std::string_view spellId) {
            activeSpells.remove(spellId, context);
        };

        // types.Actor.activeSpells(o):clear()
        activeSpellsT["clear"] = [context](MWLua::ActiveSpellsAccessor& activeSpells) { activeSpells.clear(context); };
    }

    void initMagicBindings(const Context& context)
    {
        // register user types along with their associated vector type
        auto spellT = context.mLua->sol().new_usertype<ESM::Spell>("ESM3_Spell");
        auto magicEffectT = context.mLua->sol().new_usertype<ESM::MagicEffect>("ESM3_MagicEffect");
        auto activeSpellT
            = context.mLua->sol().new_usertype<MWMechanics::ActiveSpells::ActiveSpellParams>("ActiveSpell");
        auto activeSpellEsmT
            = context.mLua->sol().new_usertype<ESM::ActiveSpells::ActiveSpellParams>("ESM3_ActiveSpell");
        auto effectParamsT = context.mLua->sol().new_usertype<ESM::ENAMstruct>("ESM3_EffectParams");
        auto activeEffectT = context.mLua->sol().new_usertype<ESM::ActiveEffect>("ESM3_ActiveEffect");

        spellT[sol::meta_function::to_string]
            = [](const ESM::Spell& rec) -> std::string { return "ESM3_Spell[" + rec.mName + "]"; };
        spellT["id"] = sol::readonly_property([](const ESM::Spell& rec) { return rec.mId; });
        spellT["name"] = sol::readonly_property([](const ESM::Spell& rec) -> std::string { return rec.mName; });
        spellT["effects"] = sol::readonly_property([context](const ESM::Spell& rec) {
            sol::table tab(context.mLua->sol(), sol::create);
            for (size_t i = 0; i < rec.mEffects.mList.size(); ++i)
                tab[i + 1] = rec.mEffects.mList[i];
            return tab;
        });

        const MWWorld::Store<ESM::MagicEffect>* magicEffectStore
            = &MWBase::Environment::get().getWorld()->getStore().get<ESM::MagicEffect>();

        // Magic effect record implementation
        magicEffectT[sol::meta_function::to_string]
            = [](const ESM::MagicEffect& rec) { return "ESM3_MagicEffect[" + rec.mId.getRefIdString() + "]"; };

        magicEffectT["id"] = sol::readonly_property([](const ESM::MagicEffect& rec) { return rec.mId; });

        magicEffectT["name"] = sol::readonly_property(
            [](const ESM::MagicEffect& rec) -> std::string { return ESM::MagicEffect::effectIdToString(rec.mIndex); });

        magicEffectT["school"]
            = sol::readonly_property([](const ESM::MagicEffect& rec) -> int { return rec.mData.mSchool; });

        magicEffectT["cost"]
            = sol::readonly_property([](const ESM::MagicEffect& rec) -> float { return rec.mData.mBaseCost; });

        magicEffectT["color"] = sol::readonly_property([](const ESM::MagicEffect& rec) -> std::vector<int> {
            return { rec.mData.mRed, rec.mData.mGreen, rec.mData.mBlue };
        });
        magicEffectT["speed"]
            = sol::readonly_property([](const ESM::MagicEffect& rec) -> float { return rec.mData.mSpeed; });

        magicEffectT["harmful"] = sol::readonly_property(
            [](const ESM::MagicEffect& rec) -> bool { return rec.mData.mFlags & ESM::MagicEffect::Harmful; });

        // ActiveSpellParams bindings
        activeSpellT[sol::meta_function::to_string] = [](const MWMechanics::ActiveSpells::ActiveSpellParams& params) {
            return "ESM3_ActiveSpell[" + params.getDisplayName() + "]";
        };
        activeSpellT["name"]
            = sol::readonly_property([](const MWMechanics::ActiveSpells::ActiveSpellParams& params) -> std::string {
                  return params.getDisplayName();
              });
        activeSpellT["effects"]
            = sol::readonly_property([context](const MWMechanics::ActiveSpells::ActiveSpellParams& params) {
                  const auto& effects = params.getEffects();
                  sol::table tab(context.mLua->sol(), sol::create);
                  for (size_t i = 0; i < effects.size(); ++i)
                      tab[i + 1] = effects[i];
                  return tab;
              });

        // ActiveEffect bindings
        activeEffectT[sol::meta_function::to_string] = [magicEffectStore](const ESM::ActiveEffect& params) {
            const ESM::MagicEffect* const rec = magicEffectStore->find(params.mEffectId);
            return "ESM3_ActiveEffect[" + ESM::MagicEffect::indexToId(rec->mIndex) + "]";
        };
        activeEffectT["name"] = sol::readonly_property([magicEffectStore](const ESM::ActiveEffect& params) {
            const ESM::MagicEffect* const rec = magicEffectStore->find(params.mEffectId);
            return "ESM3_EffectParams[" + ESM::MagicEffect::indexToId(rec->mIndex) + "]";
        });
        activeEffectT["id"]
            = sol::readonly_property([](const ESM::ActiveEffect& params) -> int { return params.mEffectId; });
        activeEffectT["magnitude"]
            = sol::readonly_property([](const ESM::ActiveEffect& params) -> float { return params.mMagnitude; });
        activeEffectT["argument"]
            = sol::readonly_property([](const ESM::ActiveEffect& params) -> int { return params.mArg; });
        activeEffectT["minMagnitude"]
            = sol::readonly_property([](const ESM::ActiveEffect& params) -> float { return params.mMinMagnitude; });
        activeEffectT["maxMagnitude"]
            = sol::readonly_property([](const ESM::ActiveEffect& params) -> float { return params.mMaxMagnitude; });
        activeEffectT["duration"]
            = sol::readonly_property([](const ESM::ActiveEffect& params) -> float { return params.mDuration; });
        activeEffectT["timeLeft"]
            = sol::readonly_property([](const ESM::ActiveEffect& params) -> float { return params.mTimeLeft; });

        // ENAMstruct (effect parameters) bindings
        effectParamsT[sol::meta_function::to_string] = [magicEffectStore](const ESM::ENAMstruct& params) {
            const ESM::MagicEffect* const rec = magicEffectStore->find(params.mEffectID);
            return "ESM3_EffectParams[" + ESM::MagicEffect::indexToId(rec->mIndex) + "]";
        };
        effectParamsT["id"]
            = sol::readonly_property([](const ESM::ENAMstruct& params) { return (int)params.mEffectID; });
        effectParamsT["name"] = sol::readonly_property([magicEffectStore](const ESM::ENAMstruct& params) {
            const ESM::MagicEffect* const rec = magicEffectStore->find(params.mEffectID);
            return "ESM3_EffectParams[" + ESM::MagicEffect::indexToId(rec->mIndex) + "]";
        });
        effectParamsT["skill"]
            = sol::readonly_property([](const ESM::ENAMstruct& params) { return (int)params.mSkill; });
        effectParamsT["attribute"]
            = sol::readonly_property([](const ESM::ENAMstruct& params) { return (int)params.mAttribute; });
        effectParamsT["range"]
            = sol::readonly_property([](const ESM::ENAMstruct& params) { return (int)params.mRange; });
        effectParamsT["area"] = sol::readonly_property([](const ESM::ENAMstruct& params) { return (int)params.mArea; });
        effectParamsT["magnMin"]
            = sol::readonly_property([](const ESM::ENAMstruct& params) { return (int)params.mMagnMin; });
        effectParamsT["magnMax"]
            = sol::readonly_property([](const ESM::ENAMstruct& params) { return (int)params.mMagnMax; });
    }
}
